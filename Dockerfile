#--------------------------------------
# Stage: Compile Apps
#--------------------------------------
FROM node:10.14.2

LABEL maintainer="quandc@vccloud.com"

ENV REFRESHED_AT 2019-01-01

WORKDIR /app

# Use package.json for Docker (without package version)
COPY package.json /app/package.json
COPY package-lock.json /app/package-lock.json

RUN npm install

COPY . /app/

ARG BUILD_ENV=staging

RUN export NODE_ENV=${BUILD_ENV} && npm run ${BUILD_ENV}


#--------------------------------------
# Stage: Packaging Apps
#--------------------------------------
FROM nginx:1.15-alpine

# VOLUME /app
WORKDIR /app

COPY --from=0 /app/dist /app

EXPOSE 80

COPY docker-entrypoint.sh /
CMD ["/docker-entrypoint.sh"]
