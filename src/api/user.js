import request from '@/utils/request'

export function getInfo() {
  return request({
    url: process.env.VUE_APP_USER_INFO_URL,
    method: 'get'
  })
}

export function fetchList(page, key, index) {
  return request({
    url: '/users',
    method: 'get',
    params: {
      page: page,
      per_page: 5,
      keyword: key,
      order_by: index
    }
  })
}

export function findUser(id) {
  return request({
    url: '/users',
    method: 'get',
    params: {
      user_id: id
    }
  })
}


