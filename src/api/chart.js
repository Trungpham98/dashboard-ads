import request from '@/utils/request'

export function fetchData(event, start, end) {
    return request({
        url: '/chart',
        methods: 'get',
        params: {
            event_category: event,
            start_date: start,
            end_date: end
        }
    })
}