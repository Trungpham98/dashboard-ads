import request from '@/utils/request'

export function fetchUcDetail(id) {
    return request({
      url: '/user_campaign/' +id,
      method: 'get',
    })
}

export function fetchListByUserID(id) {
    return request({
        url: '/user_campaign/by_user/' + id,
        method: 'get'
    })
}

export function updateStatus(id,data) {
    return request({
        url: '/user_campaign/' +id,
        method: 'patch',
        data
    })
}

export function fetchListByCampaignID(id,page) {
  return request({
      url: '/user_campaign/by_campaign/' + id,
      method: 'get',
      params:{
        page: page,
        per_page: 5,
      }
  })
}
export function createUC(data) {
    return request({
        url: 'user_campaign',
        method: 'post',
        data
    })
}
