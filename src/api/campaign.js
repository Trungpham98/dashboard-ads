import request from '@/utils/request'

export function fetchList(page,keyword,order) {
  return request({
    url: '/campaign',
    method: 'get',
    params:{
      order_by: order,
      page: page,
      per_page: 5,
      keyword: keyword,
    }
  })
}

export function fetchCampaignDetail(id) {
  return request({
    url: '/campaign/' +id,
    method: 'get',

  })
}

export function createCampaign(data) {
  return request({
    url: '/campaign',
    method: 'post',
    data
  })
}

export function updateCampaign(data) {
  return request({
    url: '/campaign',
    method: 'patch',
    data
  })
}

export function deleteCampaign(data) {
  return request({
    url: '/campaign',
    method: 'delete',
    data
  })
}

export function fetchData(query) {
  return request({
    url: '/constant',
    method: 'get',
    params: query
  })
}

export function fetchListPerPage(per_page, key) {
  return request({
    url: '/campaign',
    method: 'get',
    params: {
      per_page: per_page,
      keyword: key
    }
  })
}
