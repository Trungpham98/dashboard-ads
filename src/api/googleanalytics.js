import request from '@/utils/request'

export function fetchList(page, key, index) {
  return request({
    url: '/google_analytic',
    method: 'get',
    params:{
      page: page,
      per_page: 5,
      keyword: key,
      order_by: index
    }
  })
}

export function fetchDetail(id) {
  return request({
    url: '/google_analytic/' +id,
    method: 'get',
  })
}

export function createGoogleAnalytics(data) {
  return request({
    url: '/google_analytic',
    method: 'post',
    data
  })
}

export function updateGoogleAnalytics(data) {
  return request({
    url: '/google_analytic',
    method: 'patch',
    data
  })
}

export function fetchListPerPage(per_page, key) {
  return request({
    url: '/google_analytic',
    method: 'get',
    params: {
      per_page: per_page,
      keyword: key
    }
  })
}

export function fetchListByEC(key) {
  return request({
    url: '/google_analytic',
    method: 'get',
    params: {
      per_page: 10,
      event_category: key
    }
  })
}
