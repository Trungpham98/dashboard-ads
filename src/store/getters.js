const getters = {
  authenticated: state => state.user.authenticated,
  user: state => state.user.info,
}
export default getters
