import { getInfo, fetchList } from '@/api/user'

const state = {
  authenticated: false,
  info: {},
}

const mutations = {
  SET_AUTHENTICATED: (state) => {
    state.authenticated = true
  },
  SET_INFO: (state, data) => {
    state.info = data
  },
}

const actions = {
  // get user info
  getInfo({ commit, state }) {
    return new Promise((resolve, reject) => {
      getInfo().then(response => {

        if (!response) {
          reject('Verification failed, please Login again.')
        }
        commit('SET_INFO', response.data.data)
        commit('SET_AUTHENTICATED')
        resolve('ok')
      }).catch(error => {
        reject(error)
      })
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
