import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '*',
      name: 'Page404',
      component: () => import('./views/Error/Error.vue'),
    },
    {
      path: '/',
      name: 'Layout',
      component: () => import('@/Layout/index.vue'),
      children: [
        {
          path: '/dashboard',
          name: 'dashboard',
          component: () => import(/* webpackChunkName: "about" */ './views/Home.vue'),
        },
        {
          path: '/',
          name: 'campaign',
          component: () => import(/* webpackChunkName: "about" */ './views/Campaign/Campaign.vue'),
        },
        {
          path: '/profile',
          name: 'profile',
          component: () => import(/* webpackChunkName: "about" */ './views/Profile'),
        },
        {
          path: '/googleanalytics',
          name: 'googleanalytics',
          component: () => import(/* webpackChunkName: "about" */ './views/Google Analytics/GoogleAnalytics.vue'),
        },
        {
          path: '/addGA',
          name: 'addGA',
          component: () => import(/* webpackChunkName: "about" */ './views/Google Analytics/components/addGA.vue'),
        },
        {
          path: '/editGA/:id',
          name: 'editGA',
          component: () => import(/* webpackChunkName: "about" */ './views/Google Analytics/components/editGA.vue'),
        },
        // {
        //   path: '/detailGA/:id',
        //   name: 'detailGA',
        //   component: () => import(/* webpackChunkName: "about" */ './views/Google Analytics/components/detailGA.vue'),
        // },
        {
          path: '/addNewCampaign',
          name: 'addNewCampaign',
          component: () => import(/* webpackChunkName: "about" */ './views/Campaign/components/AddNewCampaign.vue'),
        },
        {
          path: '/editCampaign/:id',
          name: 'editCampaign',
          component: () => import(/* webpackChunkName: "about" */ './views/Campaign/components/EditCampagin.vue'),
        },
        // {
        //   path: '/detailCampaign/:id',
        //   name: 'detailCampaign',
        //   component: () => import(/* webpackChunkName: "about" */ './views/Campaign/components/DetailCampagin.vue'),
        // },
        {
          path: '/users',
          name: 'users',
          component: () => import(/* webpackChunkName: "about" */ './views/User/Users.vue'),
        },
        {
          path: '/uc/:id',
          name: 'UserCampaign',
          component: () => import(/* webpackChunkName: "about" */ './views/User/components/UserCampaign.vue'),
        },
        {
          path: '/ucByCampaignID/:id',
          name: 'UserCampaignByCampaignID',
          component: () => import(/* webpackChunkName: "about" */ './views/User/components/UserCampaignByCampaignID.vue'),
        },
        {
          path: '/report',
          name: 'report',
          component: () => import(/* webpackChunkName: "about" */ './views/Report/Report.vue'),
        }
      ]
    },
  ],
});
