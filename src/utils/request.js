import axios from 'axios'
import store from '@/store'
// import { getToken } from '@/utils/auth'

// create an axios instance
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
  // withCredentials: true, // send cookies when cross-domain requests
  timeout: 5000 ,// request timeout,
  withCredentials: true
})

// request interceptor
service.interceptors.request.use(
  config => {
    // do something before request is sent

    if (store.getters.token) {
      // let each request carry token
      // ['X-Token'] is a custom headers key
      // please modify it according to the actual situation
      config.headers['X-Token'] = getToken()
    }
    return config
  },
  error => {
    // do something with request error
    console.log(error) // for debug
    return Promise.reject(error)
  }
)

// response interceptor
service.interceptors.response.use(
  /**
   * If you want to get http information such as headers or status
   * Please return  response => response
  */

  /**
   * Determine the request status by custom code
   * Here is just an example
   * You can also judge the status by HTTP Status Code
   */
  response => {
    // if the custom code is not 200, it is judged as an error.
    // if (response.status !== 200) {
    //   return Promise.reject(new Error(response.message || 'Error'))
    // } else {
    //   return response
    // }
    console.log(response)
    return response
  },
  error => {
    console.log(parseInt(error.response.status)) // for debug
    let statusCode = parseInt(error.response.status)
    if (statusCode === 401) window.location.href = `${process.env.VUE_APP_AUTH_URL}/logout?service=${process.env.VUE_APP_DASHBOARD_URL}`;
    return error
  }
)

export default service
